﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MenuControl : MonoBehaviour {

    public GameObject MainMenu;
    private bool isInMenu = false;
    public bool IsInMenu
    {
        get { return isInMenu; }
    }

    public static MenuControl Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        /*if (GameControl.Instance.sceneOuverte.name != "MainMenu")
        {
            isInMenu = false;
        }
        else
        {
            isInMenu = true;
        }*/
        if (isInMenu)
        {
            MainMenu.GetComponent<Canvas>().enabled = true;
        }
        else
        {
            MainMenu.GetComponent<Canvas>().enabled = false;
        }
    }

    // Use this for initialization
    void Start ()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.sceneUnloaded += OnSceneUnloaded;
        if (SceneManager.GetActiveScene().name != "MainMenu" && SceneManager.GetActiveScene().name != "MainMenu2")
        {
            isInMenu = false;
        }
        else
        {
            isInMenu = true;
        }
        if (isInMenu)
        {
            MainMenu.GetComponent<Canvas>().enabled = true;
        }
        else
        {
            MainMenu.GetComponent<Canvas>().enabled = false;
        }
		
	}

    private void OnSceneUnloaded(Scene scene)
    {
        if (scene.name == "MainMenu" || scene.name == "MainMenu2")
        {
            isInMenu = false;
            MainMenu.GetComponent<Canvas>().enabled = false;
        }
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "MainMenu" || scene.name == "MainMenu2")
        {
            isInMenu = true;
            MainMenu.GetComponent<Canvas>().enabled = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>().enabled = false;
            GameObject.FindGameObjectWithTag("Player").GetComponent<InputController>().enabled = false;
        }
    }

    public void Commencer()
    {
        Gameplay.Instance.Arme = 1;
        GameControl.Instance.gameData.armeBloquee = 0;
        Gameplay.Instance.armesDebloquees.Add(1);
        Gameplay.Instance.armesDebloquees.Add(2);
        Gameplay.Instance.armesDebloquees.Add(3);
        Gameplay.Instance.NiveauJeuActuel = 2;
        Gameplay.Instance.OnRespawn();
        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
    }

    public void GoToMenu()
    {
        SceneManager.LoadSceneAsync("MainMenu");
        PauseManager.Instance.TogglePause();
        GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>().enabled = false;
        GameObject.FindGameObjectWithTag("Player").GetComponent<InputController>().enabled = false;
    }

    public void Quit()
    {
        Application.Quit();
        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
