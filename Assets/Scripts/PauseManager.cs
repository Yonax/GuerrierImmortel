﻿using UnityEngine;
using System.Collections;

public class PauseManager : Singleton<PauseManager>
{

    public GameObject PauseMenu;
    public static PauseManager Instance = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    public delegate void OnPause();
    public delegate void OnResume();

    private OnPause onPause;
    private OnResume onResume;

    [HideInInspector] public bool isPaused = false;

    public void AddToPause(IPausable p)
    {
        onPause += p.OnPause;
        onResume += p.OnResume;
    }

    public void RemoveFromPause(IPausable p)
    {
        onPause -= p.OnPause;
        onPause -= p.OnResume;
    }

    public void Pause()
    {
        if (onPause != null)
        {
            PauseMenu.GetComponent<Canvas>().enabled = true;
            onPause();
        }
        isPaused = true;
    }

    public void Resume()
    {
        if (onResume != null)
        {
            PauseMenu.GetComponent<Canvas>().enabled = false;
            onResume();
        }
        isPaused = false;
    }

    public void TogglePause()
    {
        if (!MenuControl.Instance.IsInMenu)
        {
            if (isPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }
}