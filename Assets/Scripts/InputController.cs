﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour, IPausable {
    private bool paused;

    // Use this for initialization
    void Start () {
        paused = false;
        PauseManager.I.AddToPause(this);
    }
	
	// Update is called once per frame
	void Update () {
        if (!paused)
        {
            Vector2 deplacement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            GetComponent<Joueur>().Deplacement(deplacement);
            if (Input.GetKeyDown(KeyCode.Space) && GetComponent<Joueur>().ArmeEquipee!=null)
            {
                GetComponentInChildren<Arme>().attaque();
            }
            if (Input.GetKeyDown(KeyCode.LeftControl) && GetComponent<Joueur>().EquipementEquipe != null)
            {
                GetComponent<Equipement>().utiliser();
            }
        }
    }
    public void OnPause()
    {
        paused = true;
    }

    public void OnResume()
    {
        paused = false;
    }
}
