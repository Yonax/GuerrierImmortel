﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : Equipement {
    private float deplacement;

    private float coolDownTime;
    private float coolDownTimeFin;
    public override void init()
    {

    }

    public override void utiliser()
    {
        if(coolDownTimeFin < Time.time)
        {
            switch(GetComponent<Joueur>().GetDirection) {
                case Joueur.Direction.Bas:
                    transform.position += new Vector3(0, -deplacement);
                    break;
                case Joueur.Direction.Gauche:
                    transform.position += new Vector3(-deplacement, 0);
                    break;
                case Joueur.Direction.Droite:
                    transform.position += new Vector3(deplacement, 0);
                    break;
                case Joueur.Direction.Haut:
                    transform.position += new Vector3(0, deplacement);
                    break;
                case Joueur.Direction.BasDroite:
                    transform.position += new Vector3(deplacement, -deplacement);
                    break;
                case Joueur.Direction.BasGauche:
                    transform.position += new Vector3(-deplacement, -deplacement);
                    break;
                case Joueur.Direction.HautDroite:
                    transform.position += new Vector3(deplacement, deplacement);
                    break;
                case Joueur.Direction.HautGauche:
                    transform.position += new Vector3(-deplacement, deplacement);
                    break;
            }
            coolDownTimeFin = Time.time + coolDownTime;
            GetComponent<Animator>().SetTrigger("Dash");
        }
    }

    // Use this for initialization
    void Start () {
        typeDEquipement = Type.Actif;
        deplacement = 2.0f;
        coolDownTimeFin = 0.0f;
        coolDownTime = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
