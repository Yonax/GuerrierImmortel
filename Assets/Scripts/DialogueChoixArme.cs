﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DialogueChoixArme : MonoBehaviour {

    private bool isShown;
    public bool isTriggered;
    public GameObject CanvasChoixEquipement;
    private GameObject joueur = null;

    public void ChoixArme(int choix)
    {
        GetComponent<Canvas>().enabled = false;
        transform.GetChild(Gameplay.Instance.ArmeBloquee).gameObject.SetActive(true);
        Gameplay.Instance.Arme = choix;
        CanvasChoixEquipement.GetComponent<DialogueChoixEquipement>().choiceWeaponDone = true;
        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
    }
	// Use this for initialization
	void Start () {
        isShown = false;
        isTriggered = false;
	}

	// Update is called once per frame
	void Update () {
		if (GetComponent<Canvas>().worldCamera ==null)
        {
            GetComponent<Canvas>().worldCamera = Camera.main;
        }
        if (joueur == null)
        {
            joueur = GameObject.FindGameObjectWithTag("Player");
        }
        if (isTriggered)
        {
            if (Input.GetKeyDown(KeyCode.K))
            {
                isShown = true;
                if (isShown && CanvasChoixEquipement.GetComponent<DialogueChoixEquipement>().choiceWeaponDone == false)
                {
                    GetComponent<Canvas>().enabled = true;
                    transform.GetChild(Gameplay.Instance.ArmeBloquee).gameObject.SetActive(false);
                    joueur.GetComponent<Joueur>().OnPause();
                    joueur.GetComponent<InputController>().enabled = false;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            isShown = false;
            if (!isShown)
            {
                GetComponent<Canvas>().enabled = false;
                transform.GetChild(Gameplay.Instance.ArmeBloquee).gameObject.SetActive(true);
                joueur.GetComponent<Joueur>().OnResume();
                joueur.GetComponent<InputController>().enabled = true;
            }
        }
    }
}
