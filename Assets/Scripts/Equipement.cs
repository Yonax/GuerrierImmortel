﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Equipement : MonoBehaviour {

    public enum Type
    {
        Passif,
        Actif
    }

    public enum EquipementsListe
    {
        None,
        Dash,
    }

    protected Type typeDEquipement;
    public Type TypeDEquipement { get { return typeDEquipement; } }

    [SerializeField] protected string nom;
    public string Nom
    {
        get { return nom; }
    }


    public abstract void utiliser();

    // fonction permettant de ajouter une caractéristique
    public abstract void init();


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
