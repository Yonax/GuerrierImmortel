﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockPosition : MonoBehaviour {
    public bool isBlocked = false;
    public bool isParent = false;
    private Vector3 position;

	// Use this for initialization
	void Start () {
		
	}

    void FixedUpdate()
    {
        position = transform.position;
    }
	
	// Update is called once per frame
	void LateUpdate () {
        if(isBlocked)
        {
            transform.position = position;
        }
        if (isParent)
        {
            transform.position = transform.parent.position;
        }
	}
}
