﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("EpeeHit");
        Debug.Log(collision.name);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Ok");
        Debug.Log(collision.gameObject.name);
    }
    // Update is called once per frame
    void Update () {
		
	}
}
