﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OmbreDeplacement : MonoBehaviour {

    private GameObject player = null;
    private Vector3 playerPosition;
    private Vector3 position;
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }

    }

    public void SuivreJoueur(float vitesseOmbre)
    {
        playerPosition = player.transform.position;
        playerPosition.z = 0;
        playerPosition.y -= 0.5f / 3 * player.transform.localScale.y;
        position = transform.position;
        position.z = 0;
        if ((playerPosition - position).magnitude > 0.01)
        {
            position += (playerPosition - position).normalized * vitesseOmbre * Time.deltaTime;
            
            transform.position = position;
        }
        
    }
}
