﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class Gameplay : MonoBehaviour {
    public static Gameplay Instance = null;


    public List<Vector2> origineNiveaux = new List<Vector2>();


    public GameObject armeAffichageUi;
    public Sprite[] armesSprite;

    private bool alive;
    public bool Alive
    {
        get { return alive; }
    }

    private int niveauJeuActuel;
    public int NiveauJeuActuel
    {
        get { return niveauJeuActuel; }
        set { niveauJeuActuel = value; }
    }

    private List<int> bossVaincus;
    public List<int> BossVaincus
    {
        get { return bossVaincus; }
    }

    public void AjouterBossVaincu(int i)
    {
        bossVaincus.Add(i);
    }

    public List<int> armesDebloquees;
    [SerializeField] private int armeBloquee;
    public int ArmeBloquee
    {
        get { return armeBloquee; }
    }

    [SerializeField] private int equipementBloque;
    public int EquipementBloque
    {
        get { return equipementBloque; }
    }

    public List<int> equipementsDebloques;


    public int Arme;
    public int Equipement;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(Camera.main);
        bossVaincus = new List<int>();
        armesDebloquees = new List<int>();
        equipementsDebloques = new List<int>();
        origineNiveaux.Add(Vector2.up * 2f);
        origineNiveaux.Add(Vector2.zero);
        origineNiveaux.Add(Vector2.zero);
    }
    public void OnDie()
    {
        alive = false;
        armeBloquee = Arme;
        equipementBloque = Equipement;
        niveauJeuActuel = 0;
        Arme = 0;
        Equipement = 0;
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        SceneManager.LoadSceneAsync("Niveau0");
    }

    public void OnRespawn()
    {
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        SceneManager.LoadSceneAsync("Niveau" + niveauJeuActuel + "");
        alive = true;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        bossVaincus = GameControl.Instance.gameData.indexBossVaincus;
        armesDebloquees = GameControl.Instance.gameData.armesDebloquees;
        equipementsDebloques = GameControl.Instance.gameData.equipementsDebloques;
        Arme = GameControl.Instance.gameData.arme;
        armeBloquee = GameControl.Instance.gameData.armeBloquee;
        Equipement = GameControl.Instance.gameData.equipement;
        equipementBloque = GameControl.Instance.gameData.equipementBloque;
    }
	// Use this for initialization
	void Start () {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
	
	// Update is called once per frame
	void Update () {
        if(niveauJeuActuel == 2)
        {
            armeAffichageUi.SetActive(true);
            armeAffichageUi.GetComponent<Image>().sprite = armesSprite[Arme - 1];
        }
        else
        {
            armeAffichageUi.SetActive(false);
        }
        
    }
}
