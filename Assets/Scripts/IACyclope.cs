﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IACyclope : MonoBehaviour {

    [SerializeField]
    private GameObject ombre;
    [SerializeField]
    private GameObject viseur;
    [SerializeField]
    private GameObject onde;
    [SerializeField]
    private GameObject laser;
    [SerializeField]
    private float pointsDeVieMax;
    private float pointsDeVie;
    [SerializeField]
    private Slider barreDeVie;
    [SerializeField]
    private Image fillBarreDeVie;
    [SerializeField]
    private float tempsEntreLesAttaques;
    [SerializeField]
    private float tempsEntreLesAttaquesMidLife;
    [SerializeField]
    private float tempsEntreLesAttaquesQuartLife;
    private float finDerniereAttaque ;
    public float FinDerniereAttaque
    {
        set { finDerniereAttaque = value; }
    }
    private GameObject player = null;
    [SerializeField]
    private float distanceMinAttaqueDistance;
    private CyclopeAttaques.AttaqueEnCoursCyclope derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.Aucune;
    private int compteurMemeAttaque = 1;
    [SerializeField]
    private int memeAttaqueMax;
    [SerializeField]
    private float tempsEnRouge;
    [SerializeField]
    private float tempsPourQueLeRougeDisparaisse;
    private float debutRouge;
    private bool enRouge = false;
    [SerializeField]
    private float chanceSautCorpsACorps;
    [SerializeField]
    private AudioClip blesser;
    private bool rampage = false;
    [SerializeField]
    private bool canRampage;
    [SerializeField]
    private float tempsPourMourir;
    private float debutMort;
    private bool mouru = false;
    [SerializeField]
    private float resistancePresqueMort;
    
	// Use this for initialization
	void Start () {
        barreDeVie.maxValue = pointsDeVieMax;
        pointsDeVie = pointsDeVieMax;
        barreDeVie.value = pointsDeVie;
	}
	
	// Update is called once per frame
	void Update () {
        if(pointsDeVie < pointsDeVieMax / 4f)
        {
            fillBarreDeVie.color = new Color(0.33f, 0, 0);
            if(!canRampage)
            {
                tempsEntreLesAttaques = tempsEntreLesAttaquesQuartLife;
            }
            
            if(canRampage && !rampage)
            {
                rampage = true;
                compteurMemeAttaque = 3;
            }
        }
        else if(pointsDeVie < pointsDeVieMax / 2f)
        {
            fillBarreDeVie.color = new Color(0.66f, 0, 0);
            tempsEntreLesAttaques = tempsEntreLesAttaquesMidLife;
        }
        /*if(Input.GetKeyDown(KeyCode.I))
        {
            SubitDegats(50);
        }*/
        if(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        else
        {
            if(player.transform.position.y > transform.position.y + 1f / 8f * transform.localScale.y)
            {
                player.GetComponent<SpriteRenderer>().sortingOrder = 1;
            }
            else
            {
                player.GetComponent<SpriteRenderer>().sortingOrder = 11;
            }
        }
        barreDeVie.value = pointsDeVie;
        if (pointsDeVie <= 0)
        {
            if(!mouru)
            {
                laser.SetActive(false);
                onde.SetActive(false);
                viseur.SetActive(false);
                GetComponent<BoxCollider2D>().enabled = false;
                debutMort = Time.time;
                mouru = true;
            }
            Mort();
            
        }

        if(enRouge && Time.time - debutRouge > tempsEnRouge)
        {
            
            float couleur = (Time.time - debutRouge - tempsEnRouge) / tempsPourQueLeRougeDisparaisse;
            GetComponent<SpriteRenderer>().color = new Color(1,couleur,couleur);
        }
        if(enRouge && Time.time - debutRouge > tempsEnRouge + tempsPourQueLeRougeDisparaisse)
        {
            GetComponent<SpriteRenderer>().color = Color.white;
            enRouge = false;
        }        

        if (!mouru && GetComponent<CyclopeAttaques>().AttaqueEnCours == CyclopeAttaques.AttaqueEnCoursCyclope.Aucune && (Time.time - finDerniereAttaque) > tempsEntreLesAttaques)
        {
            ChoixAttaque();
        }
	}

    void ChoixAttaque()
    {
        if(!rampage)
        {
            if ((player.transform.position - transform.position).magnitude < distanceMinAttaqueDistance)//Cac
            {
                if (Random.value < (1 - chanceSautCorpsACorps) / 2)
                {
                    if (derniereAttaque == CyclopeAttaques.AttaqueEnCoursCyclope.Laser)
                    {
                        if (compteurMemeAttaque >= memeAttaqueMax)
                        {
                            GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.FrappeSol);
                            compteurMemeAttaque = 1;
                            derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.FrappeSol;
                        }
                        else
                        {
                            compteurMemeAttaque++;
                            GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.Laser);
                            derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.Laser;
                        }

                    }
                    else
                    {
                        GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.Laser);
                        compteurMemeAttaque = 1;
                        derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.Laser;
                    }

                }
                else if (Random.value < 1 - chanceSautCorpsACorps)
                {
                    if (derniereAttaque == CyclopeAttaques.AttaqueEnCoursCyclope.FrappeSol)
                    {
                        if (compteurMemeAttaque >= memeAttaqueMax)
                        {
                            GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.Laser);
                            compteurMemeAttaque = 1;
                            derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.Laser;
                        }
                        else
                        {
                            compteurMemeAttaque++;
                            GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.FrappeSol);
                            derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.FrappeSol;
                        }

                    }
                    else
                    {
                        GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.FrappeSol);
                        compteurMemeAttaque = 1;
                        derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.FrappeSol;
                    }
                }
                else
                {
                    GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.Saut);
                    compteurMemeAttaque = 1;
                    derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.Saut;
                }
            }
            else//Distance
            {
                if (Random.value < 0.5)
                {
                    if (derniereAttaque == CyclopeAttaques.AttaqueEnCoursCyclope.Laser)
                    {
                        if (compteurMemeAttaque >= memeAttaqueMax)
                        {
                            GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.Saut);
                            compteurMemeAttaque = 1;
                            derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.Saut;
                        }
                        else
                        {
                            compteurMemeAttaque++;
                            GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.Laser);
                            derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.Laser;
                        }

                    }
                    else
                    {
                        GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.Laser);
                        compteurMemeAttaque = 1;
                        derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.Laser;
                    }

                }
                else
                {
                    if (derniereAttaque == CyclopeAttaques.AttaqueEnCoursCyclope.Saut)
                    {
                        if (compteurMemeAttaque >= memeAttaqueMax)
                        {
                            GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.Laser);
                            compteurMemeAttaque = 1;
                            derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.Laser;
                        }
                        else
                        {
                            compteurMemeAttaque++;
                            GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.Saut);
                            derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.Saut;
                        }

                    }
                    else
                    {
                        GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.Saut);
                        compteurMemeAttaque = 1;
                        derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.Saut;
                    }
                }
            }
        }
        else
        {
            if(compteurMemeAttaque >= 3)
            {
                
                tempsEntreLesAttaques = tempsEntreLesAttaquesQuartLife;
                float rand = Random.value;
                if(rand <= 0.33)
                {
                    GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.Saut);
                    derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.Saut;
                    compteurMemeAttaque = 1;
                }
                else if(rand <= 0.66)
                {
                    GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.FrappeSol);
                    derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.FrappeSol;
                    compteurMemeAttaque = 1;
                }
                else
                {
                    GetComponent<CyclopeAttaques>().Rampage = false;
                    GetComponent<CyclopeAttaques>().LanceAttaque(CyclopeAttaques.AttaqueEnCoursCyclope.Laser);
                    derniereAttaque = CyclopeAttaques.AttaqueEnCoursCyclope.Laser;
                    compteurMemeAttaque = 1;

                }
            }
            else
            {
                GetComponent<CyclopeAttaques>().Rampage = true;
                GetComponent<CyclopeAttaques>().LanceAttaque(derniereAttaque);
                compteurMemeAttaque++;
                if(compteurMemeAttaque>=3)
                {
                    tempsEntreLesAttaques = tempsEntreLesAttaquesMidLife;
                }
            }
        }
    }

    public void SubitDegats(float degats)
    {

        GetComponent<AudioSource>().PlayOneShot(blesser);
        GetComponent<SpriteRenderer>().color = Color.red;
        debutRouge = Time.time;
        enRouge = true;
        if(pointsDeVie < pointsDeVieMax / 4f)
        {
            degats = degats / resistancePresqueMort;
        }
        pointsDeVie -= degats;

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        try
        {
            collision.GetComponent<Joueur>().SubitDegats(1,transform.position - collision.transform.position);
        }
        catch {
        }
    }
    private void Mort()
    {
        float couleur = 1- (Time.time - debutMort) / tempsPourMourir;
        if (couleur < 0)
        {
            couleur = 0;
        }
        Debug.Log(couleur);
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, couleur);
        if (Time.time - debutMort > tempsPourMourir)
        {
            ombre.SetActive(false);
            laser.SetActive(false);
            onde.SetActive(false);
            viseur.SetActive(false);
            gameObject.SetActive(false);
        }
    }
}
