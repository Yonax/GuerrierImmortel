﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DialogueChoixEquipement : MonoBehaviour
{

    private bool isShown;
    public bool isTriggered;
    public bool choiceWeaponDone;
    private GameObject joueur = null;

    public void ChoixArme(int choix)
    {
        GetComponent<Canvas>().enabled = false;
        choiceWeaponDone = false;
        transform.GetChild(Gameplay.Instance.EquipementBloque).gameObject.SetActive(true);
        Gameplay.Instance.Equipement = choix;
        Gameplay.Instance.NiveauJeuActuel = Gameplay.Instance.BossVaincus.Count + 2;
        Gameplay.Instance.OnRespawn();
        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
        joueur.GetComponent<Joueur>().OnResume();
        joueur.GetComponent<InputController>().enabled = true;
    }
    // Use this for initialization
    void Start()
    {
        isShown = false;
        isTriggered = false;
        choiceWeaponDone = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Canvas>().worldCamera == null)
        {
            GetComponent<Canvas>().worldCamera = Camera.main;
        }
        if (joueur == null)
        {
            joueur = GameObject.FindGameObjectWithTag("Player");
        }
        if (isTriggered && choiceWeaponDone)
        {
            isShown = true;
            if (isShown)
            {
                GetComponent<Canvas>().enabled = true;
                transform.GetChild(Gameplay.Instance.Equipement).gameObject.SetActive(false);
                joueur.GetComponent<Joueur>().OnPause();
                joueur.GetComponent<InputController>().enabled = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            isShown = false;
            if (!isShown)
            {
                choiceWeaponDone = false;
                GetComponent<Canvas>().enabled = false;
                transform.GetChild(Gameplay.Instance.EquipementBloque).gameObject.SetActive(true);
                joueur.GetComponent<Joueur>().OnResume();
                joueur.GetComponent<InputController>().enabled = true;
            }
        }
    }
}
