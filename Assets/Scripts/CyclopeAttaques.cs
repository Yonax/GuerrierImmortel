﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyclopeAttaques : MonoBehaviour {
    [SerializeField]
    private GameObject oeil;
    [SerializeField]
    private GameObject ondeDeChoc;
    public enum AttaqueEnCoursCyclope { Saut, Laser, FrappeSol, Aucune}
    private AttaqueEnCoursCyclope attaqueEnCours;
    public AttaqueEnCoursCyclope AttaqueEnCours
    {
        get { return attaqueEnCours; }
    }
    private int phaseAttaqueEnCours;
    public int PhaseEnCours
    {
        get { return phaseAttaqueEnCours; }
    }
    private Vector2 position;
    [SerializeField]
    private float vitesseSaut;
    public float yMaxSortieEcran;
    [SerializeField]
    private GameObject ombre;
    [SerializeField]
    private float vitesseOmbre;
    [SerializeField]
    private float vitesseChute;
    [SerializeField]
    private float tempsDuSuivitDeLOmbre;
    private float debutSuivitDeLOmbre;
    [SerializeField]
    private GameObject viseurLaser;
    private Vector2 positionViseurLaser;
    [SerializeField]
    private float tempsDuSuivitDuLaser;
    [SerializeField]
    private float tempsDuSuivitLaserRampage;
    private float debutDuSuivitDuLaser;
    [SerializeField]
    private float vitesseViseurLaser;
    private GameObject player = null;
    [SerializeField]
    private GameObject laser;
    [SerializeField]
    private float dureeLaser;
    [SerializeField]
    private float dureeLaserRampage;
    private float debutTirLaser;
    private Vector2 distanceLaser;
    [SerializeField]
    private float dureeOndeDeChocLaser;
    [SerializeField]
    private float tailleOndeDeChocLaser;
    private float debutOndeDeChocLaser;
    [SerializeField]
    private float dureeAnimationFrappe;
    private float debutAnimationFrappe;
    [SerializeField]
    private float dureeOndeDeChocFrapppe;
    private float debutOndeDeChocFrappe;
    private bool frappeCommencee = false;
    [SerializeField]
    private float tailleOndeDeChocFrappe;
    [SerializeField]
    private AudioClip chargeLaser;
    [SerializeField]
    private AudioClip tireLaser;
    [SerializeField]
    private AudioClip frappeSol;
    [SerializeField]
    private AudioClip atterissage;
    [SerializeField]
    private AudioClip saut;
    private bool sonSautLance = false;
    private bool rampage;
    public bool Rampage
    {
        set { rampage = value; }
    }


    // Use this for initialization
    void Start () {
        attaqueEnCours = AttaqueEnCoursCyclope.Aucune;
	}
	
	// Update is called once per frame
	void Update () {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        position = transform.position;
        /*if(Input.GetKeyDown(KeyCode.O) && attaqueEnCours == AttaqueEnCoursCyclope.Aucune)
        {
            attaqueEnCours = AttaqueEnCoursCyclope.Saut;
            phaseAttaqueEnCours = 1;
        }
        if(Input.GetKeyDown(KeyCode.L) && attaqueEnCours == AttaqueEnCoursCyclope.Aucune)
        {
            attaqueEnCours = AttaqueEnCoursCyclope.Laser;
            phaseAttaqueEnCours = 1;
        }
        if(Input.GetKeyDown(KeyCode.K) && attaqueEnCours == AttaqueEnCoursCyclope.Aucune)
        {
            attaqueEnCours = AttaqueEnCoursCyclope.FrappeSol;
            phaseAttaqueEnCours = 1;
        }*/
        if (attaqueEnCours == AttaqueEnCoursCyclope.Saut)
        {
            Saut();
        }
        if(attaqueEnCours == AttaqueEnCoursCyclope.Laser)
        {
            Laser();
        }
        if(attaqueEnCours == AttaqueEnCoursCyclope.FrappeSol)
        {
            FrappeAuSol();
        }

        transform.position = position;
	}

    private void Saut()
    {
        GetComponent<Animator>().SetInteger("AttaqueEnCours", 0);
        GetComponent<Animator>().SetInteger("PhaseEnCours", phaseAttaqueEnCours);
        ombre.GetComponent<Animator>().SetInteger("AttaqueEnCours", 0);
        ombre.GetComponent<Animator>().SetInteger("PhaseEnCours", phaseAttaqueEnCours);
        switch (phaseAttaqueEnCours)
        {
            case 1://saute
                if(!sonSautLance)
                {
                    GetComponent<AudioSource>().PlayOneShot(saut);
                    sonSautLance = true;
                }
                GetComponent<BoxCollider2D>().enabled = false;
                position.y += vitesseSaut * Time.deltaTime;
                if(position.y >= yMaxSortieEcran)
                {
                    GetComponent<SpriteRenderer>().enabled = false;
                    phaseAttaqueEnCours = 2;
                    debutSuivitDeLOmbre = Time.time;
                    sonSautLance = false;
                }
                break;
            case 2://vise le joueur
                ombre.GetComponent<OmbreDeplacement>().SuivreJoueur(vitesseOmbre);
                if(Time.time - debutSuivitDeLOmbre > tempsDuSuivitDeLOmbre)
                {
                    phaseAttaqueEnCours = 3;
                    position = transform.position;
                    position.x = ombre.transform.position.x;
                    GetComponent<SpriteRenderer>().enabled = true;
                }
                break;
            case 3://tombe
                position.y -= vitesseChute * Time.deltaTime;
                if(position.y <= ombre.transform.position.y + 0.05)
                {
                    GetComponent<AudioSource>().PlayOneShot(atterissage);
                    GetComponent<BoxCollider2D>().enabled = true;
                    phaseAttaqueEnCours = 0;
                    attaqueEnCours = AttaqueEnCoursCyclope.Aucune;
                    GetComponent<IACyclope>().FinDerniereAttaque = Time.time;
                    Vector3 posOmbre = ombre.transform.position;
                    posOmbre.y -= 0.05f;
                    posOmbre.z = 0;
                    ombre.transform.position = posOmbre;
                    GetComponent<Animator>().SetInteger("AttaqueEnCours", 3);
                    GetComponent<Animator>().SetInteger("PhaseEnCours", 0);
                    ombre.GetComponent<Animator>().SetInteger("AttaqueEnCours", 3);
                    ombre.GetComponent<Animator>().SetInteger("PhaseEnCours", 0);

                }
                break;
            default:
                Debug.Log("Erreur dans la phase du Saut : phase en cours : " + phaseAttaqueEnCours);
                break;
        }
    }
    private void Laser()
    {
        GetComponent<Animator>().SetInteger("AttaqueEnCours", 1);
        GetComponent<Animator>().SetInteger("PhaseEnCours", phaseAttaqueEnCours);
        ombre.GetComponent<Animator>().SetInteger("AttaqueEnCours", 1);
        ombre.GetComponent<Animator>().SetInteger("PhaseEnCours", phaseAttaqueEnCours);
        switch (phaseAttaqueEnCours)
        {
            case 1://Vise le joueur
                
                if (!viseurLaser.activeSelf)
                {
                    viseurLaser.SetActive(true);
                    viseurLaser.transform.position = player.transform.position;
                    viseurLaser.transform.localScale = transform.localScale * 2f;
                    debutDuSuivitDuLaser = Time.time;
                    GetComponent<AudioSource>().PlayOneShot(chargeLaser);
                }
                viseurLaser.GetComponent<ViseurLaserDeplacement>().SuivreJoueur(vitesseViseurLaser);
                if(rampage)
                {
                    viseurLaser.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, (1f * (Time.time - debutDuSuivitDuLaser)) / tempsDuSuivitLaserRampage);
                    viseurLaser.transform.localScale = (Time.time - debutDuSuivitDuLaser) / tempsDuSuivitLaserRampage * (1f / 3f * player.transform.localScale - transform.localScale * 2f) + transform.localScale * 2f;
                }
                else
                {
                    viseurLaser.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, (1f * (Time.time - debutDuSuivitDuLaser)) / tempsDuSuivitDuLaser);
                    viseurLaser.transform.localScale = (Time.time - debutDuSuivitDuLaser) / tempsDuSuivitDuLaser * (1f / 3f * player.transform.localScale - transform.localScale * 2f) + transform.localScale * 2f;
                }
               
                if (Time.time - debutDuSuivitDuLaser > tempsDuSuivitDuLaser || (rampage && Time.time - debutDuSuivitDuLaser > tempsDuSuivitLaserRampage))
                {
                    phaseAttaqueEnCours = 2;
                    oeil.GetComponent<OeilDirection>().oeilVerrouiller = true;
                    
                    debutTirLaser = Time.time;
                }
                
                
                break;

            case 2://tire le laser
                if (!laser.activeSelf)
                {
                    laser.SetActive(true);
                    laser.transform.rotation = new Quaternion();
                    distanceLaser = viseurLaser.transform.position - oeil.transform.position;
                    float angle =270- (Mathf.Atan2(distanceLaser.x,distanceLaser.y) * 180 / Mathf.PI);
                    Vector2 positionLazer = distanceLaser / 2f + (Vector2)oeil.transform.position;
                    float scaleXLaser = 2 * distanceLaser.magnitude;
                    laser.transform.Rotate(new Vector3(0, 0, angle));
                    laser.transform.position = positionLazer;
                    laser.transform.localScale = new Vector3(scaleXLaser, 1, 1);
                    positionViseurLaser = viseurLaser.transform.position;
                    viseurLaser.SetActive(false);
                    GetComponent<AudioSource>().PlayOneShot(tireLaser);
                }
                if(rampage)
                {
                    laser.transform.position = (Time.time - debutTirLaser) / dureeLaserRampage * distanceLaser / 2f + (Vector2)oeil.transform.position;
                    laser.transform.localScale = new Vector3((Time.time - debutTirLaser) / dureeLaserRampage * 2 * distanceLaser.magnitude, 1, 1);
                }
                else
                {
                    laser.transform.position = (Time.time - debutTirLaser) / dureeLaser * distanceLaser / 2f + (Vector2)oeil.transform.position;
                    laser.transform.localScale = new Vector3((Time.time - debutTirLaser) / dureeLaser * 2 * distanceLaser.magnitude, 1, 1);
                }
                
                if (Time.time - debutTirLaser > dureeLaser || (rampage && Time.time - debutTirLaser > dureeLaserRampage))
                {
                    oeil.GetComponent<OeilDirection>().oeilVerrouiller = false;
                    laser.SetActive(false);
                    phaseAttaqueEnCours = 3;
                    debutOndeDeChocLaser = Time.time;
                }
                break;
            case 3://onde de choc
                if(!ondeDeChoc.activeSelf)
                {
                    ondeDeChoc.SetActive(true);
                    GetComponent<AudioSource>().PlayOneShot(atterissage);
                }
                ondeDeChoc.GetComponent<OndeDeChoc>().ExplosionALaPosition(positionViseurLaser, tailleOndeDeChocLaser * (Time.time - debutOndeDeChocLaser) / dureeOndeDeChocLaser);
                ondeDeChoc.GetComponent<CapsuleCollider2D>().enabled = true;
                if((Time.time - debutOndeDeChocLaser) > dureeOndeDeChocLaser)
                {
                    ondeDeChoc.GetComponent<CapsuleCollider2D>().enabled = false;
                    ondeDeChoc.SetActive(false);
                    phaseAttaqueEnCours = 0;
                    attaqueEnCours = AttaqueEnCoursCyclope.Aucune;
                    GetComponent<IACyclope>().FinDerniereAttaque = Time.time;
                    GetComponent<Animator>().SetInteger("AttaqueEnCours", 3);
                    GetComponent<Animator>().SetInteger("PhaseEnCours", 0);
                    ombre.GetComponent<Animator>().SetInteger("AttaqueEnCours", 3);
                    ombre.GetComponent<Animator>().SetInteger("PhaseEnCours", 0);
                }
                break;

            default:
                Debug.Log("Erreur dans la phase du Laser : phase en cours : " + phaseAttaqueEnCours);
                break;
        }
    }

    private void FrappeAuSol()
    {
        GetComponent<Animator>().SetInteger("AttaqueEnCours", 2);
        GetComponent<Animator>().SetInteger("PhaseEnCours", phaseAttaqueEnCours);
        ombre.GetComponent<Animator>().SetInteger("AttaqueEnCours", 2);
        ombre.GetComponent<Animator>().SetInteger("PhaseEnCours", phaseAttaqueEnCours);
        switch (phaseAttaqueEnCours)
        {
            case 1://animation
                if(!frappeCommencee)
                {
                    frappeCommencee = true;
                    debutAnimationFrappe = Time.time;
                }
                if(Time.time - debutAnimationFrappe > dureeAnimationFrappe)
                {
                    frappeCommencee = false;
                    phaseAttaqueEnCours = 2;
                    debutOndeDeChocFrappe = Time.time;
                }
                break;
            case 2://frappe
                if(!ondeDeChoc.activeSelf)
                {
                    ondeDeChoc.SetActive(true);
                    GetComponent<AudioSource>().PlayOneShot(frappeSol);
                }
                ondeDeChoc.GetComponent<OndeDeChoc>().ExplosionALaPosition(transform.position, tailleOndeDeChocFrappe * (Time.time - debutOndeDeChocFrappe) / dureeOndeDeChocFrapppe);
                ondeDeChoc.GetComponent<CapsuleCollider2D>().enabled = true;
                if (Time.time - debutOndeDeChocFrappe > dureeOndeDeChocFrapppe)
                {
                    ondeDeChoc.GetComponent<CapsuleCollider2D>().enabled = false;
                    ondeDeChoc.SetActive(false);
                    phaseAttaqueEnCours = 0;
                    attaqueEnCours = AttaqueEnCoursCyclope.Aucune;
                    GetComponent<IACyclope>().FinDerniereAttaque = Time.time;
                    GetComponent<Animator>().SetInteger("AttaqueEnCours", 3);
                    GetComponent<Animator>().SetInteger("PhaseEnCours", 0);
                    ombre.GetComponent<Animator>().SetInteger("AttaqueEnCours", 3);
                    ombre.GetComponent<Animator>().SetInteger("PhaseEnCours", 0);
                }
                break;

            default:
                Debug.Log("Erreur dans la phase de la frappe au sol : phase en cours : " + phaseAttaqueEnCours);
            break;
        }
    }
    public bool LanceAttaque(AttaqueEnCoursCyclope attaque)
    {
        if(attaqueEnCours == AttaqueEnCoursCyclope.Aucune && attaque!=AttaqueEnCoursCyclope.Aucune)
        {
            attaqueEnCours = attaque;
            phaseAttaqueEnCours = 1;
            return true;
        }
        else
        {
            return false;
        }
    }
}
