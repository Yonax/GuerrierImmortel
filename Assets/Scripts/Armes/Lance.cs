﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lance : Arme
{

    public override void attaque()
    {
        if (Time.time > tempsDerniereAttaque + delai)
        {
            tempsDerniereAttaque = Time.time;
            Joueur.Direction direction = transform.parent.gameObject.GetComponent<Joueur>().DirectionPrecedent;
            transform.parent.gameObject.GetComponent<Animator>().SetTrigger("AttaqueLance");
        }
    }

    public override void attaqueSpeciale()
    {
        if (Time.time > tempsDerniereAttaque + delai)
        {
            tempsDerniereAttaque = Time.time;
            Joueur.Direction direction = transform.parent.gameObject.GetComponent<Joueur>().DirectionPrecedent;
            transform.parent.gameObject.GetComponent<Animator>().SetInteger("AttaqueSpecialeLance", (int)direction);
        }
    }
    // Use this for initialization
    void Start()
    {
        nom = "Lance";
        degats = 15f;
        delai = 0.6f;
        tempsDeplacementBloque = 0.4f;
        maxEnergieSpeciale = 100f;
        tauxRafraichissementSpecial = 1f;
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        Refresh();
    }
}
