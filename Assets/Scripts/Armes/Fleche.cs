﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fleche : Arme {
    private float timeInactive = 0.3f;
    private float timeOutInactive;
    private float vitesse = 8.0f;
    public Vector3 direction;

    public override void attaque()
    {

    }

    public override void attaqueSpeciale()
    {

    }
    // Use this for initialization
    void Start()
    {
        nom = "Fleche";
        degats = 17f;
        delai = 1.8f;
        tempsDeplacementBloque = 0.3f;
        maxEnergieSpeciale = 100f;
        tauxRafraichissementSpecial = 1f;
        timeOutInactive = Time.time + timeInactive;
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > timeOutInactive)
        {
            transform.position += vitesse * Time.deltaTime * direction;
        }   
        Refresh();
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        try
        {
            collision.gameObject.GetComponent<IACyclope>().SubitDegats(degats);
            transform.parent.gameObject.GetComponent<Animator>().SetTrigger("Degat");
            Destroy(gameObject);
        }
        catch { }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
