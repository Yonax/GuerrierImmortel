﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Epee : Arme {

    public override void attaque()
    {
        if (Time.time > tempsDerniereAttaque + delai)
        {
            tempsDerniereAttaque = Time.time;
            Joueur.Direction direction = transform.parent.gameObject.GetComponent<Joueur>().DirectionPrecedent;
            transform.parent.gameObject.GetComponent<Animator>().SetTrigger("AttaqueEpee");
        }
    }

    public override void attaqueSpeciale()
    {
        if (Time.time > tempsDerniereAttaque + delai)
        {
            tempsDerniereAttaque = Time.time;
            Joueur.Direction direction = transform.parent.gameObject.GetComponent<Joueur>().DirectionPrecedent;
            transform.parent.gameObject.GetComponent<Animator>().SetInteger("AttaqueSpecialeEpee", (int)direction);
        }
    }
    // Use this for initialization
    void Start ()
    {
        nom = "Epee";
        degats = 20f;
        delai = 0.8f;
        tempsDeplacementBloque = 0.6f;
        maxEnergieSpeciale = 100f;
        tauxRafraichissementSpecial = 1f;
        Init();
    }
	
	// Update is called once per frame
	void Update ()
    {
        Refresh();
	}

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
        transform.parent.gameObject.GetComponent<Animator>().SetTrigger("Degat");
    }
}
