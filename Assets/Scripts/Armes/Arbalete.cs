﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arbalete : Arme
{
    private GameObject flecheBas;
    private GameObject flecheHaut;
    private GameObject flecheGauche;
    private GameObject flecheDroite;
    public Joueur.Direction direction;

    public override void attaque()
    {
        if (Time.time > tempsDerniereAttaque + delai)
        {
            tempsDerniereAttaque = Time.time;
            direction = transform.parent.gameObject.GetComponent<Joueur>().DirectionPrecedent;
            transform.parent.gameObject.GetComponent<Animator>().SetTrigger("AttaqueArbalete");
            switch(direction)
            {
                case Joueur.Direction.Haut:
                    Instantiate(flecheHaut, transform.parent.transform.position, new Quaternion()).GetComponent<Fleche>().direction = new Vector3(0,1);
                    break;
                case Joueur.Direction.Bas:
                    Instantiate(flecheBas, transform.parent.transform.position, new Quaternion()).GetComponent<Fleche>().direction = new Vector3(0, -1);
                    break;
                case Joueur.Direction.Droite:
                    Instantiate(flecheDroite, transform.parent.transform.position, new Quaternion()).GetComponent<Fleche>().direction = new Vector3(1, 0);
                    break;
                case Joueur.Direction.Gauche:
                    Instantiate(flecheGauche, transform.parent.transform.position, new Quaternion()).GetComponent<Fleche>().direction = new Vector3(-1, 0);
                    break;
            }
        }
    }

    public override void attaqueSpeciale()
    {
        if (Time.time > tempsDerniereAttaque + delai)
        {
            tempsDerniereAttaque = Time.time;
            Joueur.Direction direction = transform.parent.gameObject.GetComponent<Joueur>().DirectionPrecedent;
            transform.parent.gameObject.GetComponent<Animator>().SetInteger("AttaqueSpecialeEpee", (int)direction);
        }
    }
    // Use this for initialization
    void Start()
    {
        nom = "Arbalete";
        degats = 10f;
        delai = 1.8f;
        tempsDeplacementBloque = 0.3f;
        maxEnergieSpeciale = 100f;
        tauxRafraichissementSpecial = 1f;
        flecheBas = Resources.Load("FlecheBas") as GameObject;
        flecheHaut = Resources.Load("FlecheHaut") as GameObject;
        flecheDroite = Resources.Load("FlecheDroite") as GameObject;
        flecheGauche = Resources.Load("FlecheGauche") as GameObject;
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        Refresh();
    }
}
