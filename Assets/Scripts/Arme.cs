﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Arme : MonoBehaviour {
    public enum ArmesListe
    {
        Fist,
        Epee,
        Lance,
        Arbalete,
    }

	[SerializeField] protected string nom;
	public string Nom {
		get { return nom; }
	}
    [SerializeField] protected float degats;
	public float Degats {
		get { return degats; }
    }
    [SerializeField] protected float tempsDerniereAttaque;
    [SerializeField] public float TempsDerniereAttaque
    {
        get { return tempsDerniereAttaque; }
    }
    protected float delai;
    public float Delai
    {
        get { return delai; }
    }
    [SerializeField] protected float tempsDeplacementBloque;
    public float TempsDeplacementBloque
    {
        get { return tempsDeplacementBloque; }
    }
    [SerializeField] protected float maxEnergieSpeciale;
    public float MaxEnergieSpeciale
    {
        get { return maxEnergieSpeciale; }
    }
    [SerializeField] protected float energieSpeciale;
    public float EnergieSpeciale
    {
        get { return energieSpeciale; }
        set { energieSpeciale = value; }
    }
    [SerializeField] protected float tauxRafraichissementSpecial;

    public abstract void attaque ();
	public abstract void attaqueSpeciale ();

    public void Init()
    {
        energieSpeciale = maxEnergieSpeciale;
    }

    public void Refresh()
    {
        if (energieSpeciale + tauxRafraichissementSpecial * Time.deltaTime >= maxEnergieSpeciale)
        {
            energieSpeciale = maxEnergieSpeciale;
        }
        else
        {
            energieSpeciale += tauxRafraichissementSpecial * Time.deltaTime;
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        try
        {
            collision.gameObject.GetComponent<IACyclope>().SubitDegats(degats);
        }
        catch { }
    }

}
