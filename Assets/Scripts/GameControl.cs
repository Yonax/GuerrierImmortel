﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

/// <summary>
/// Class for handling persistent game data
/// </summary>
public class GameControl : MonoBehaviour
{
    #region Public Fields

    public static GameControl Instance;
    public GameData gameData = new GameData();
    public Scene sceneOuverte;
    

    private string cheminSauvegarde;
    public string CheminSauvegarde
    {
        get
        {
            if (cheminSauvegarde != null)
                return cheminSauvegarde;
            else
            {
                cheminSauvegarde = Application.persistentDataPath + "/SavedGames/";
                return cheminSauvegarde;
            }
        }
    }

    #endregion Public Fields

    #region Private Fields

    private const string FILE_EXTENSION = ".xml";

    // Save Load Data
    private string fichierSauvegarde;

    #endregion Private Fields

    #region Public Methods

    /// <summary>
    /// Deletes the save file if it exists and errors out otherwise.
    /// </summary>
    /// <param name="fichierSauvegarde"></param>
    public void SupprimerFichierSauvegarde(string fichierSauvegarde)
    {
        if (File.Exists(CheminSauvegarde + fichierSauvegarde + FILE_EXTENSION))
        {
            File.Delete(CheminSauvegarde + fichierSauvegarde + FILE_EXTENSION);
        }
        else
        {
            Debug.LogError("Failed to delete non existant file " + CheminSauvegarde + fichierSauvegarde + FILE_EXTENSION);
        }
    }

    /// <summary>
    /// Checks if the save file exists in the file system
    /// </summary>
    /// <param name="nomFichierTest"></param>
    /// <returns>True if it exists and false otherwise</returns>
    public bool LeFichierExiste(string nomFichierTest)
    {
        foreach (GameData data in ObtenirTousFichiersSauvegarde())
        {
            if (data.dernierFichierSauvegarde == nomFichierTest)
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Create a new file name, check for existing files of same player name
    /// </summary>
    /// <returns></returns>
    public string GenererNouveauNomSauvegarde()
    {
        int tentative = 0;
        string nouveauNomSauvegarde = "";

        while (nouveauNomSauvegarde == "")
        {
            // Save Name is Player Name
            string stringVerification = gameData.nomJoueur;

            // Add a number if original already taken
            if (tentative != 0) stringVerification += tentative;

            if (!File.Exists(CheminSauvegarde + stringVerification))
            {
                // Make the check string the new file name
                nouveauNomSauvegarde = stringVerification;
            }

            tentative++;
        }

        return nouveauNomSauvegarde;
    }

    /// <summary>
    /// Gets a list of all save files in the save directory.
    /// </summary>
    /// <returns></returns>
    public List<GameData> ObtenirTousFichiersSauvegarde()
    {
        List<GameData> toutesSauvegardes = new List<GameData>();

        // Check Save Path
        foreach (string nomFichier in Directory.GetFiles(CheminSauvegarde))
        {
            // Get Player Data for Each File
            toutesSauvegardes.Add(ObtenirFichierSauvegarde(nomFichier));
        }

        return toutesSauvegardes;
    }

    /// <summary>
    /// Finds the value associated with the flag
    /// </summary>
    /// <param name="flagName"></param>
    /// <returns></returns>
    public int ObtenirFlags(string flagName)
    {
        GameFlag flag = gameData.gameFlags.Find(x => x.flag == flagName);

        // Create Non-existant flags but default to 0
        if (flag == null)
        {
            SetFlag(flagName, 0);
            return 0;
        }

        return flag.valeur;
    }

    /// <summary>
    /// Checks if a particular level has been cleared yet or not
    /// </summary>
    /// <param name="niveau">Level to check</param>
    /// <returns>True if cleared and false otherwise</returns>
    public bool ObtenirNiveauFini(int niveau)
    {
        return ObtenirFlags("niveau" + niveau + "cleared") == 1 ? true : false;
    }

    /// <summary>
    /// Load game data from file for active use
    /// </summary>
    /// <param name="gameName"></param>
    /// <returns></returns>
    public void LoadGame(string gameName)
    {
        VerifierChemin();

        // Assemble path to file to load game from
        String cheminFichierComplet = CheminSauvegarde + gameName + FILE_EXTENSION;

        if (File.Exists(cheminFichierComplet))
        {
            // Put it into a file
            Debug.Log("Deserializing " + cheminFichierComplet);

            FileStream fs = File.Open(cheminFichierComplet, FileMode.Open);

            // Deserialize the XML Save File (Using XmlSerializer instead of BinarySerializer)
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(GameData));
            XmlReader reader = XmlReader.Create(fs);
            gameData = xmlSerializer.Deserialize(reader) as GameData;
            fs.Close();

            // Loads the scene from which the game was saved
            SceneManager.LoadSceneAsync(gameData.savedScene, LoadSceneMode.Single);
            PauseManager.Instance.Resume();
        }
        else
        {
            Debug.Log("Failed to save to file " + cheminFichierComplet);
        }
        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
    }

    /// <summary>
    /// Save all game data to file
    /// </summary>
    /// <param name="fichierSauvegarde"></param>
    public void SaveGame(string fichierSauvegarde)
    {
        VerifierChemin();

        // Update fichierSauvegarde name
        if (fichierSauvegarde == null)
        {
            fichierSauvegarde = GenererNouveauNomSauvegarde();
        }

        this.fichierSauvegarde = fichierSauvegarde;

        // FileStream fs = File.Create(GameDic.Instance.CheminSauvegarde + fichierSauvegarde);

        UpdateSaveData(fichierSauvegarde);

        string cheminSauvegardeEntier = CheminSauvegarde + fichierSauvegarde + FILE_EXTENSION;

        FileStream fs;

        // Create a file or open an old one up for writing to
        if (!File.Exists(cheminSauvegardeEntier))
        {
            fs = File.Create(cheminSauvegardeEntier);
        }
        else
        {
            fs = File.OpenWrite(cheminSauvegardeEntier);
        }

        XmlSerializer serializer = new XmlSerializer(typeof(GameData));
        TextWriter textWriter = new StreamWriter(fs);
        serializer.Serialize(textWriter, gameData);
        fs.Close();

        // Bug fix : XMLSerializer écrit parfois trop à la dernière ligne ce qui cause des erreurs lors du save ou load
        string [] lignes = File.ReadAllLines(cheminSauvegardeEntier);
        using (StreamWriter sw = new StreamWriter(cheminSauvegardeEntier))
        {
            int fin = -1;
            for(int i = 0; i<lignes.Length; i++)
            {
                if (i == lignes.Length - 1 || lignes[i].Contains("</GameData>"))
                {
                    sw.WriteLine("</GameData>");
                    fin = i;
                }
                else if (fin == -1 && fin <= i)
                {
                    sw.WriteLine(lignes[i]);
                }
                else
                {
                    sw.WriteLine("");
                }
            }
        }
        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
        Debug.Log("Game Saved to " + cheminSauvegardeEntier);
    }

    /// <summary>
    /// Set Current Save Related Information on gameData
    /// </summary>
    /// <param name="fichierSauvegarde"></param>
    private void UpdateSaveData(string fichierSauvegarde)
    {
        gameData.salleActuelle = Gameplay.Instance.NiveauJeuActuel;
        //gameData.positionJoueur = GameObject.Find("Joueur").transform.position;
        gameData.positionJoueur = Gameplay.Instance.origineNiveaux[gameData.salleActuelle];
        gameData.armesDebloquees = Gameplay.Instance.armesDebloquees;
        gameData.equipementsDebloques = Gameplay.Instance.equipementsDebloques;
        gameData.arme = Gameplay.Instance.Arme;
        gameData.armeBloquee = Gameplay.Instance.ArmeBloquee;
        gameData.equipement = Gameplay.Instance.Equipement;
        gameData.equipementBloque = Gameplay.Instance.EquipementBloque;
        gameData.dernierFichierSauvegarde = fichierSauvegarde;
        gameData.dernierTempsSauvegarde = DateTime.Now.ToBinary();
        gameData.savedScene = SceneManager.GetActiveScene().name;
    }

    // For flag storing and getting
    public void SetFlag(string flagName, int valeur)
    {
        // Overwrite Old Key/Values
        GameFlag oldFlag = gameData.gameFlags.Find(x => x.flag == flagName);

        // Either update the value or add a new one if it does not exist
        if (oldFlag != null)
        {
            oldFlag.valeur = valeur;
        }
        else
        {
            // Does not exist in list
            gameData.gameFlags.Add(new GameFlag(flagName, valeur));
        }
    }

    #endregion Public Methods

    #region Private Methods

    /// <summary>
    /// Checks if the file has not yet been created
    /// </summary>
    /// <param name="fichierSauvegarde"></param>
    /// <returns></returns>
    private bool IsNewFile(string fichierSauvegarde)
    {
        return !File.Exists(CheminSauvegarde + fichierSauvegarde + FILE_EXTENSION);
    }

    /// <summary>
    /// Initialization
    /// </summary>
    private void Awake()
    {
        //Check if instance already exists
        if (Instance == null)
        {
            //if not, set instance to this
            Instance = this;

            // Find objects on level - necessary to call directly for first load
            SceneManager.sceneLoaded += OnSceneLoaded;
            sceneOuverte = SceneManager.GetActiveScene();
        }

        //If instance already exists and it's not this:
        else if (Instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        // Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Checks to see if the CheminSauvegarde directory exists and creates a new one of it does not.
    /// </summary>
    private void VerifierChemin()
    {
        // Check if directory exists, if not create it
        if (!Directory.Exists(CheminSauvegarde))
        {
            Directory.CreateDirectory(CheminSauvegarde);
        }
    }

    /// <summary>
    /// Retrieves the data stored inside of a save file
    /// </summary>
    /// <param name="cheminFichierComplet"></param>
    /// <returns></returns>
    private GameData ObtenirFichierSauvegarde(string cheminFichierComplet)
    {
        if (File.Exists(cheminFichierComplet))
        {
            // Old Binary Formmater Method BinaryFormatter bf = new BinaryFormatter(); FileStream
            // fs = File.Open(fullFilePath, FileMode.Open);

            // Put it into a file PlayerData data = (PlayerData)bf.Deserialize(fs);

            // fs.Close();

            // XML SERIALIZER TEST INSTEAD OF BINARYFORMATTER
            FileStream fs = File.Open(cheminFichierComplet, FileMode.Open);

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(GameData));
            XmlReader reader = XmlReader.Create(fs);
            GameData data = xmlSerializer.Deserialize(reader) as GameData;
            fs.Close();

            return data;
        }
        else
        {
            Debug.LogError("Failed to save to file " + cheminFichierComplet);
            return null;
        }
    }

    /// <summary>
    /// Make sure that the save / load directory exists.
    /// </summary>
    /// <param name="scene"></param>
    /// <param name="mode"></param>
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        SaveGame("sauvegarde01");
        VerifierChemin();
    }

    #endregion Private Methods
}

[Serializable]
public class GameFlag
{
    #region Public Fields

    public string flag;
    public int valeur;

    #endregion Public Fields

    #region Public Constructors

    public GameFlag()
    {
    }

    public GameFlag(string flag, int valeur)
    {
        this.flag = flag;
        this.valeur = valeur;
    }

    #endregion Public Constructors
}

[Serializable]
public class GameData
{
    #region Public Fields

    public int salleActuelle;

    public List<GameFlag> gameFlags;

    public List<int> indexBossVaincus;

    public List<int> equipementsDebloques;

    public List<int> armesDebloquees;

    public int arme;
    public int armeBloquee;

    public int equipement;
    public int equipementBloque;

    public int vie;

    public string nomJoueur;

    // Needs properties to access
    [NonSerialized]
    public Vector2 positionJoueur;

    public string dernierFichierSauvegarde;

    public long dernierTempsSauvegarde;

    public string savedScene;

    #endregion Public Fields

    #region Public Constructors

    /// <summary>
    /// Default Constructor for New Game - Contains Starting Stats
    /// </summary>
    public GameData()
    {
        positionJoueur = Vector2.zero;
        vie = 3;
        nomJoueur = "Cham";
        salleActuelle = 1;
        savedScene = "";
        gameFlags = new List<GameFlag>();
        indexBossVaincus = new List<int>();
        equipementsDebloques = new List<int>();
        armesDebloquees = new List<int>();
        arme = 0;
        armeBloquee = 0;
        equipement = 0;
        equipementBloque = 0;
    }

    #endregion Public Constructors

    #region Public Properties

    // Can't serialize a vector so needs to be broken down into 2 properties
    public float PositionJoueurX
    {
        get
        {
            return positionJoueur.x;
        }
        set
        {
            positionJoueur.x = value;
        }
    }

    public float PositionJoueurY
    {
        get
        {
            return positionJoueur.y;
        }
        set
        {
            positionJoueur.y = value;
        }
    }

    #endregion Public Properties
}