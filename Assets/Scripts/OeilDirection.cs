﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OeilDirection : MonoBehaviour {

    private GameObject player = null;
    private GameObject cyclope = null;
    public Vector2 position;
    public bool oeilVerrouiller;
    private float remiseANiveau;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


        position = transform.localPosition;
		if(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        if(cyclope == null)
        {
            cyclope = GameObject.Find("Cyclope");
        }
        if((player != null && cyclope != null) && !oeilVerrouiller)
        {

            if (cyclope.GetComponent<CyclopeAttaques>().AttaqueEnCours == CyclopeAttaques.AttaqueEnCoursCyclope.Saut && cyclope.GetComponent<CyclopeAttaques>().PhaseEnCours == 3)
            {
                remiseANiveau = -0.15f;
            }
            else if (cyclope.GetComponent<CyclopeAttaques>().AttaqueEnCours == CyclopeAttaques.AttaqueEnCoursCyclope.FrappeSol && cyclope.GetComponent<CyclopeAttaques>().PhaseEnCours == 1)
            {
                remiseANiveau = 0.03f;
            }
            else if (cyclope.GetComponent<CyclopeAttaques>().AttaqueEnCours == CyclopeAttaques.AttaqueEnCoursCyclope.FrappeSol && cyclope.GetComponent<CyclopeAttaques>().PhaseEnCours == 2)
            {
                remiseANiveau = -0.085f;
            }
            else
            {
                remiseANiveau = 0;
            }

            //Position base.
            //transform.localScale = cyclope.transform.localScale;

            float y = 0.46f;

            //Position avancée.
            float rayon =  0.15f / 2f;

            position = (player.transform.position - new Vector3(cyclope.transform.position.x,y + cyclope.transform.position.y)).normalized * rayon;

            position.y += y + remiseANiveau;
        }



        transform.localPosition = position;
	}
}
