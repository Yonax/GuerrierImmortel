﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderDialogue : MonoBehaviour {

    public GameObject dialogue;
    public GameObject dialogue2;


    void OnTriggerEnter2D(Collider2D other)
    {
        dialogue.GetComponent<DialogueChoixArme>().isTriggered = true;
        dialogue2.GetComponent<DialogueChoixEquipement>().isTriggered = true;
    }
    void OnTriggerExit2D(Collider2D other)
    {
        dialogue.GetComponent<DialogueChoixArme>().isTriggered = false;
        dialogue2.GetComponent<DialogueChoixEquipement>().isTriggered = false;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
