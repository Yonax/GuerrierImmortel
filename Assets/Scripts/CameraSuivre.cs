﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSuivre : MonoBehaviour {
    private GameObject joueur = null;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if(joueur == null)
        {
            joueur = GameObject.FindGameObjectWithTag("Player");
        } else
        {
            Vector3 newPosition = transform.position;
            Vector3 joueurPosition = joueur.transform.position;
            Vector3 direction = joueurPosition - newPosition;
            float distance = direction.magnitude;
            //newPosition.x = joueur.transform.position.x;
            //newPosition.y = joueur.transform.position.y;
            if(distance > 0.01f)
            {
                newPosition.x = newPosition.x + direction.x * 0.05f;
                newPosition.y = newPosition.y + direction.y * 0.05f;
                transform.position = newPosition;
            }
        }
	}
}
