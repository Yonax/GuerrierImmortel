﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager> {

    public bool loading;

    private GameObject joueur = null;
    public static GameManager Instance = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
           else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseManager.Instance.TogglePause();
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            Gameplay.Instance.Arme = 2;
            Gameplay.Instance.NiveauJeuActuel = 2;
            Gameplay.Instance.OnRespawn();  
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            joueur.GetComponent<Joueur>().SubitDegats(3, Vector2.zero);
        }
        if (joueur == null)
        {
            joueur = GameObject.FindGameObjectWithTag("Player");
            DontDestroyOnLoad(joueur);
        }
	}
}
