﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour {

    private static GameplayManager instance;

    public static GameplayManager Instance
    {
        get
        {
            if(instance == null)
            {
                instance = new GameplayManager();
            }
            return instance;
        }
    }
    public List<GameObject> armes;

	// Use this for initialization
	void Start () {
		if(instance == null)
        {
            instance = new GameplayManager();
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
