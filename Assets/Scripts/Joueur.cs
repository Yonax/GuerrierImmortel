﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Joueur : MonoBehaviour, IPausable{

    private bool paused;
    [SerializeField]
	private int maxVie = 3;
	public int MaxVie {
		get { return maxVie; }
	}

    private int vie;
    public int Vie
    {
        get { return vie; }
    }
    [SerializeField]
    private List<GameObject> coeurs;
    [SerializeField]
    private Sprite coeurBrise;
    [SerializeField]
    private Sprite coeurEntier;
    [SerializeField]
    private float vitesse;
	public float Vitesse {
		get { return vitesse; }
	}

    private bool mouvementPossible;

    [SerializeField]
    private float dureeClignotement;
    private float debutClignotement;
    [SerializeField]
    private int nombreClignotements;
    private bool clignote = false;
    [SerializeField]
    private float forceDeRepoussement;
    [SerializeField]
    private AudioClip toucher;

    private Vector2 position;

    private Arme arme;
    public Arme ArmeEquipee
    {
        get { return arme; }
    }

    private Equipement accessoire;
    public Equipement EquipementEquipe
    {
        get { return accessoire; }
    }

    public enum Direction { Haut, HautDroite, Droite, BasDroite, Bas, BasGauche, Gauche, HautGauche}

    private Direction directionPrecedent;
    public Direction DirectionPrecedent { get { return directionPrecedent; } }
    private Direction direction;
    public Direction GetDirection
    {
        get { return direction; }
    }
    


	// Use this for initialization
	void Start ()
    {
        vie = maxVie;
        if(coeurs.Count < vie)
        {
            Debug.Log("Pas assez de coeurs pour le joueur");
        }
        if(coeurs.Count > vie)
        {
            int i = 1;
            foreach(GameObject coeur in coeurs)
            {
                if(i>vie)
                {
                    coeur.SetActive(false);
                }
                i++;
            }
        }
        mouvementPossible = true;
        direction = Direction.Droite;
        paused = false;
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.sceneUnloaded += OnSceneUnloaded;
        PauseManager.I.AddToPause(this);
        ResetVie();
        /*if (MenuControl.Instance.IsInMenu)
        {
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<InputController>().enabled = false;
        }
        else
        {
            GetComponent<SpriteRenderer>().enabled = true;
            GetComponent<InputController>().enabled = true;
        }*/

        string nomArme = (Enum.GetValues(Arme.ArmesListe.Fist.GetType())).GetValue(Gameplay.Instance.Arme).ToString();
        arme = (Arme)transform.GetChild(0).gameObject.AddComponent(Type.GetType(nomArme));
        
        string nomAccessoire = (Enum.GetValues(Equipement.EquipementsListe.Dash.GetType())).GetValue(Gameplay.Instance.Equipement).ToString();
        accessoire = (Equipement)gameObject.AddComponent(Type.GetType(nomAccessoire));

    }

    private void OnSceneUnloaded(Scene scene)
    {
        if ((scene.name != "MainMenu" || scene.name != "Niveau0") && scene.name != "Preview Scene")
        {
            Destroy(arme);
            Destroy(accessoire);
        }
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (MenuControl.Instance.IsInMenu)
        {
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<InputController>().enabled = false;
        }
        else
        {
            GetComponent<SpriteRenderer>().enabled = true;
            GetComponent<InputController>().enabled = true;
        }
        ResetVie();
        string nomArme = (Enum.GetValues(Arme.ArmesListe.Fist.GetType())).GetValue(Gameplay.Instance.Arme).ToString();
        arme = (Arme)transform.GetChild(0).gameObject.AddComponent(Type.GetType(nomArme));

        transform.position = new Vector3(GameControl.Instance.gameData.PositionJoueurX, GameControl.Instance.gameData.PositionJoueurY);

        string nomAccessoire = (Enum.GetValues(Equipement.EquipementsListe.Dash.GetType())).GetValue(Gameplay.Instance.Equipement).ToString();
        accessoire = (Equipement)gameObject.AddComponent(Type.GetType(nomAccessoire));
    }

    // Update is called once per frame
    void Update () {
        //Maj des coeurs
        
        int i = 1;
        foreach(GameObject coeur in coeurs)
        {
            if (MenuControl.Instance.IsInMenu)
            {
                coeur.SetActive(false);
            }
            else
            {
                coeur.SetActive(true);
                if (i > vie)
                {
                    if(i > maxVie)
                    {
                        coeur.SetActive(false);
                    }
                    else
                    {
                        coeur.SetActive(true);
                        coeur.GetComponent<Image>().sprite = coeurBrise;
                    }
                    
                }
                i++;
            }
        }

        if (!paused)
        {
            if (arme == null)
            {
                mouvementPossible = true;
            }
            else if (arme.TempsDerniereAttaque + arme.TempsDeplacementBloque < Time.time)
            {
                mouvementPossible = true;
            }
            else
            {
                mouvementPossible = false;
            }
        }

        if(vie <= 0)
        {
            ResetVie();
            Gameplay.Instance.OnDie();
        }
		
        if(clignote)
        {
            float couleur = (Time.time - debutClignotement) / dureeClignotement * nombreClignotements % 1;
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, couleur);
        }
        if( clignote && Time.time - debutClignotement > dureeClignotement)
        {
            clignote = false;
            GetComponent<SpriteRenderer>().color = Color.white;
        }

	}

    public void Deplacement(Vector2 directionMouvement)
    {
        if (mouvementPossible)
        {
            position = transform.position;
            if (directionMouvement.magnitude != 0)
            {
                position += directionMouvement * vitesse / directionMouvement.magnitude * Time.deltaTime;
            }

            float epsilon = 0.001f;

            if (Math.Abs(directionMouvement.x) > epsilon && Math.Abs(directionMouvement.y) < epsilon)
            {
                if (directionMouvement.x > 0)
                {
                    direction = Direction.Droite;
                }
                if (directionMouvement.x < 0)
                {
                    direction = Direction.Gauche;
                }
            }
            else if (Math.Abs(directionMouvement.y) > epsilon && Math.Abs(directionMouvement.x) < epsilon)
            {
                if (directionMouvement.y > 0)
                {
                    direction = Direction.Haut;
                }
                if (directionMouvement.y < 0)
                {
                    direction = Direction.Bas;
                }
            }
            else if (Math.Abs(directionMouvement.x) > epsilon && Math.Abs(directionMouvement.y) > epsilon)
            {
                if (directionMouvement.x > epsilon && directionMouvement.y > epsilon)
                {
                    direction = Direction.HautDroite;
                }
                else if (directionMouvement.x > epsilon && directionMouvement.y < epsilon)
                {
                    direction = Direction.BasDroite;
                }
                else if (directionMouvement.x < epsilon && directionMouvement.y < epsilon)
                {
                    direction = Direction.BasGauche;
                }
                else if (directionMouvement.x < epsilon && directionMouvement.y > epsilon)
                {
                    direction = Direction.HautGauche;
                }
            }


            if (directionMouvement.magnitude != 0)
            {
                GetComponent<Animator>().SetBool("Marche", true);
            }
            else
            {
                GetComponent<Animator>().SetBool("Marche", false);
            }

            switch (direction)
            {
                case Direction.Gauche:
                case Direction.Haut:
                case Direction.Droite:
                case Direction.Bas:
                    directionPrecedent = direction;
                    GetComponent<Animator>().SetInteger("Direction", (int)direction);
                    break;
                case Direction.BasDroite:
                    if (directionPrecedent != Direction.Bas && directionPrecedent != Direction.Droite)
                    {
                        if (UnityEngine.Random.value < 0.5f)
                        {
                            directionPrecedent = Direction.Bas;
                            GetComponent<Animator>().SetInteger("Direction", (int)Direction.Bas);
                        }
                        else
                        {
                            directionPrecedent = Direction.Droite;
                            GetComponent<Animator>().SetInteger("Direction", (int)Direction.Droite);
                        }
                    }
                    break;
                case Direction.BasGauche:
                    if (directionPrecedent != Direction.Bas && directionPrecedent != Direction.Gauche)
                    {
                        if (UnityEngine.Random.value < 0.5f)
                        {
                            directionPrecedent = Direction.Bas;
                            GetComponent<Animator>().SetInteger("Direction", (int)Direction.Bas);
                        }
                        else
                        {
                            directionPrecedent = Direction.Gauche;
                            GetComponent<Animator>().SetInteger("Direction", (int)Direction.Gauche);
                        }
                    }
                    break;
                case Direction.HautGauche:
                    if (directionPrecedent != Direction.Haut && directionPrecedent != Direction.Gauche)
                    {
                        if (UnityEngine.Random.value < 0.5f)
                        {
                            directionPrecedent = Direction.Gauche;
                            GetComponent<Animator>().SetInteger("Direction", (int)Direction.Gauche);
                        }
                        else
                        {
                            directionPrecedent = Direction.Haut;
                            GetComponent<Animator>().SetInteger("Direction", (int)Direction.Haut);
                        }
                    }
                    break;
                case Direction.HautDroite:
                    if (directionPrecedent != Direction.Haut && directionPrecedent != Direction.Droite)
                    {
                        if (UnityEngine.Random.value < 0.5f)
                        {
                            directionPrecedent = Direction.Haut;
                            GetComponent<Animator>().SetInteger("Direction", (int)Direction.Haut);
                        }
                        else
                        {
                            directionPrecedent = Direction.Droite;
                            GetComponent<Animator>().SetInteger("Direction", (int)Direction.Droite);
                        }
                    }
                    break;
            }

            //AABB
            if(position.x < -10.2f)
            {
                position.x = -10.2f;
            }
            if(position.y > 6.3f)
            {
                position.y = 6.3f;
            }
            if(position.x > 11.5f)
            {
                position.x = 11.5f;
            }
            if(position.y < -4.95f)
            {
                position.y = -4.95f;
            }
            
            transform.position = position;
        }
        else
        {
            GetComponent<Animator>().SetBool("Marche", false);
        }
    }

    public void OnPause()
    {
        paused = true;
        Time.timeScale = 0;
    }

    public void OnResume()
    {
        paused = false;
        Time.timeScale = 1;
    }

    public void SubitDegats(int degats, Vector2 directionRepoussement)
    {
        if(!clignote)
        {
            GetComponent<AudioSource>().PlayOneShot(toucher);
            vie -= degats;
            debutClignotement = Time.time;
            clignote = true;
            transform.position = (Vector2)transform.position - forceDeRepoussement * Time.deltaTime * directionRepoussement.normalized;
        }
        
    }
    
    public void ResetVie()
    {
        vie = maxVie;
        if (coeurs.Count < vie)
        {
            Debug.Log("Pas assez de coeurs pour le joueur");
        }
        if (coeurs.Count > vie)
        {
            int i = 1;
            foreach (GameObject coeur in coeurs)
            {
                coeur.GetComponent<Image>().sprite = coeurEntier;
                if (i > vie)
                {
                    coeur.SetActive(false);
                }
                i++;
            }
        }
    }
}
