﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OndeDeChoc : MonoBehaviour {

    [SerializeField]
    private float ratio;// scale.x = ratio * scale.y
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ExplosionALaPosition(Vector2 positionExplosion, float scale)
    {
        transform.position = positionExplosion;
        transform.localScale = new Vector3(ratio * scale, scale);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        try
        {
            collision.GetComponent<Joueur>().SubitDegats(1, transform.position - collision.transform.position);
        }
        catch { }
    }
}
